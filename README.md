(Villafañe)Trabajo Multidisciplinario
### 1.4.0 Trabajo multidisciplinario (Análisis, GIT, SCRUM, OOP)

    1) Crear tareas usando alguna herramienta informática, como ser trello. 
    Se creará una tarea por cada método, en la misma se deberá explicar que funcionalidad tendrá ese método, que tipos de datos recibe, que tipo de datos devuelve  y que validaciones va a hacer.

    2) Se organizarán las tareas en historias, una historia por cada clase a desarrollar.

    3) Se organizarán los sprints, un sprint cada 5 días (se pone este corte, solo a los fines pedagógicos, en la práctica se debe hacer mas espaciado).

    4) Al momento de desarrollar se debe hacer un commit por cada tarea, osea por cada método.
    La entrega se hace adjuntando un google doc con dos links, uno debe ser el link al repositorio y el otro link debe ser a la herramienta online usada para organizar las tareas.