﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tarea004remake
{
	class DatosVenta
	{
		public decimal MontoDeVenta { get; set; }
		public int CantEmpleados { get; set; }
		public int CantClientes { get; set; }
		public string Sucursal { get; set; }

		public decimal montodeventa
		{
			get { return MontoDeVenta; }
			set
			{
				MontoDeVenta = value < 0 ? 0 : value;
			}
		}

		public DatosVenta(decimal mont, int cante, int cantc, string sucur)
		{
			MontoDeVenta = mont;
			CantEmpleados = cante;
			CantClientes = cantc;
			Sucursal = sucur;
		}

		public DatosVenta(){}

		DatosVenta[] datos = new DatosVenta[99];
		public void IngresarDatos(int cant)
		{
			string[] sucursales = new string[] 
			{
				"Once", 
				"San Nicolas", 
				"Almagro", 
				"Microcentro", 
				"Palermo", 
				"San Cristobal", 
				"Parque Patricios"
			};

			for (int i = 0; i < cant; i++)
			{
				Console.WriteLine("Viaje -> {0}", i+1);
				datos[i] = new DatosVenta();
				Console.Write("Monto de la Venta: ");
				datos[i].MontoDeVenta = decimal.Parse(Console.ReadLine());
				Console.Write("Cantidad de Empleados: ");
				datos[i].CantEmpleados = int.Parse(Console.ReadLine());
				Console.Write("Cantidad de Clientes: ");
				datos[i].CantClientes = int.Parse(Console.ReadLine());
				//Eligue la sucursal
				StringBuilder sb = new StringBuilder();
				sb.AppendLine("Elija una sucursal: ");
				sb.AppendLine("1. Once");
				sb.AppendLine("2. San Nicolas");
				sb.AppendLine("3. Almagro");
				sb.AppendLine("4. Microcentro");
				sb.AppendLine("5. Palermo");
				sb.AppendLine("6. San Cristobal");
				sb.AppendLine("7. Parque Patricios");
				sb.Append("Opcion: ");
				Console.Write(sb.ToString());
				int op = int.Parse(Console.ReadLine());
				#region switch op
				switch (op)
				{
					case 1:
						datos[i].Sucursal = sucursales[0];
						break;
					case 2:
						datos[i].Sucursal = sucursales[1];
						break;
					case 3:
						datos[i].Sucursal = sucursales[2];
						break;
					case 4:
						datos[i].Sucursal = sucursales[3];
						break;
					case 5:
						datos[i].Sucursal = sucursales[4];
						break;
					case 6:
						datos[i].Sucursal = sucursales[5];
						break;
					case 7:
						datos[i].Sucursal = sucursales[6];
						break;
					default:
						Console.WriteLine("Eliga una opcion valida");
						return;
				}
				#endregion
				Console.Write("\n");
			}
		}

		decimal promVentas;
		public void PromedioDeVentas(int cant)
		{
			for(int i=0; i<=cant; i++)
			{
				promVentas += datos[i].MontoDeVenta;
			}
			decimal finalVentas = promVentas / cant;
			Console.WriteLine("Promedio de Ventas: {0}", finalVentas);
		}

		int promEmpleados;
		public void PromedioEmpleados(int cant)
		{
			for(int i=0; i<=cant; i++)
			{
				promEmpleados += datos[i].CantEmpleados;
			}
			int finalEmpleados = promEmpleados / cant;
			Console.WriteLine("Promedio de Empleados: {0}", finalEmpleados);
		}

		decimal ventaTotal;
		public void VentaTotal(int cant)
		{
			for (int i=0; i<=cant; i++)
			{
				ventaTotal += datos[i].MontoDeVenta;
			}
			Console.WriteLine("Venta total: {0}", ventaTotal);
		}

		decimal max;
		string sucur1;
		public void VentaMaxima(int cant)
		{
			for (int i=0; i<=cant; i++)
			{
				if(datos[i].MontoDeVenta > max)
				{
					max = datos[i].MontoDeVenta;
					sucur1 = datos[i].Sucursal;
				}
			}
			Console.WriteLine("Venta Maxima: {0}\nSucursal: {1}", max, sucur1);
		}

		decimal min;
		string sucur2;
		public void VentaMinima(int cant)
		{
			min = max;
			for(int i=0; i<=cant; i++)
			{
				if(datos[i].MontoDeVenta < min)
				{
					min = datos[i].MontoDeVenta;
					sucur2 = datos[i].Sucursal;
				}
			}
			Console.WriteLine("Venta Minima: {0}\nSucursal: {1}", min, sucur2);
		}

	}
}